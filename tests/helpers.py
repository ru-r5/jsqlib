""" This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

Created on Aug 17, 2021

@author: pymancer@gmail.com (polyanalitika.ru)
"""
from typing import List
from collections import Counter

RULES = [f"L{'0' * (3 - len(str(abs(r))))}{r}" for r in range(1, 48)]


def filter_rules(exclude: List[str]) -> List[str]:
    return list((Counter(RULES) - Counter(exclude)).elements())
