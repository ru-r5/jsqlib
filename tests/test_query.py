""" This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

Created on Jul 29, 2021

@author: pymancer@gmail.com (polyanalitika.ru)
"""
import pytest

from typing import List
from jsonschema import Draft201909Validator, Draft202012Validator
from jsonschema.exceptions import ValidationError
from jsqlib import Query
from jsqlib.core import PGBuilder
from jsqlib.helpers.types import SCBND_T
from .helpers import filter_rules


def test_validate():
    json = """{
  "query": {
    "select": [
      {
        "eval": 1
      }
    ]
  }
}
"""
    query = Query(raw=json)
    query.validate()


def test_validate_custom():
    schema='{}'
    json = """{
  "query": {
    "select": [
      {
        "eval": 1
      }
    ]
  }
}
"""
    query = Query(raw=json, schema=schema)
    query.validate()
    assert isinstance(query._validator, Draft202012Validator)


def test_validate_specific_draft():
    schema='{"$schema": "https://json-schema.org/draft/2019-09/schema"}'
    json = """{
  "query": {
    "select": [
      {
        "eval": 1
      }
    ]
  }
}
"""
    query = Query(raw=json, schema=schema)
    query.validate()
    assert isinstance(query._validator, Draft201909Validator)


def test_validate_custom_dict():
    schema=dict()
    json = """{
  "query": {
    "select": [
      {
        "eval": 1
      }
    ]
  }
}
"""
    query = Query(raw=json, schema=schema)
    query.validate()


def test_validate_select():
    schema="""{
  "type": "object",
  "unevaluatedProperties": false,
  "properties": {
    "query": {
      "type": "object",
      "unevaluatedProperties": false,
      "properties": {
        "select": {
          "type": "array",
          "unevaluatedItems": false,
          "items": {
            "type": "object",
            "unevaluatedProperties": false,
            "properties": {
              "name": {
                "type": "string"
              }
            }
          }
        }
      }
    }
  }
}"""
    json = """{
  "query": {
    "select": [
      {
        "name": "foo"
      }
    ]
  }
}
"""
    query = Query(raw=json, schema=schema)
    query.validate()


def test_validate_no_query():
    schema="""{
  "type": "object",
  "unevaluatedProperties": false,
  "properties": {
    "select": {
      "type": "array",
      "unevaluatedItems": false,
      "items": {
        "type": "object",
        "unevaluatedProperties": false,
        "properties": {
          "name": {
            "type": "string"
          }
        }
      }
    }
  }
}"""
    json = """{
  "select": [
    {
      "name": "foo"
    }
  ]
}
"""
    query = Query(raw=json, schema=schema)
    query.validate()


def test_validate_error():
    schema="""{
  "type": "object",
  "unevaluatedProperties": false,
  "properties": {
    "query": {
      "type": "object",
      "unevaluatedProperties": false,
      "properties": {
        "select": {
          "type": "array",
          "unevaluatedItems": false,
          "items": {
            "type": "object",
            "unevaluatedProperties": false,
            "properties": {
              "name": {
                "type": "string"
              }
            }
          }
        }
      }
    }
  }
}"""
    json = """{
  "query": {
    "select": [
      {
        "bad": 1
      }
    ]
  }
}
"""
    query = Query(raw=json, schema=schema)
    with pytest.raises(ValidationError):
        query.validate()


def test_validate_no_select_error():
    schema="""{
  "type": "object",
  "unevaluatedProperties": false,
  "properties": {
    "query": {
      "type": "object",
      "unevaluatedProperties": false,
      "properties": {
        "select": {
          "type": "array",
          "unevaluatedItems": false,
          "items": {
            "type": "object",
            "unevaluatedProperties": false,
            "properties": {
              "name": {
                "type": "string"
              }
            }
          }
        }
      }
    }
  }
}"""
    json = """{
  "query": {
    "bad": [
      {
        "name": "foo"
      }
    ]
  }
}
"""
    query = Query(raw=json, schema=schema)
    with pytest.raises(ValidationError):
        query.validate()


def test_select_as_dict():
    sql = """select 1
"""
    raw = {
  'query': {
    'select': [
      {
        'eval': 1
      }
    ]
  }
}
    query = Query(raw=raw)
    assert sql == query.prettify()


def test_select():
    sql = """select 1
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": 1
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_select_aliased():
    sql = """select 1 as "id"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": 1,
        "alias": "id"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_select_bare():
    sql = """select 1
"""
    json = """{
  "select": [
    {
      "eval": 1
    }
  ]
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_calc():
    sql = """select ((1 + 2) - 3)
"""
    json = """{
  "select": [
    {
      "eval": {
        "sub": [
          {
            "add": [
              1,
              2
            ]
          },
          3
        ]
      }
    }
  ]
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_cast():
    sql = """select cast(1 as varchar)
"""
    json = """{
  "select": [
    {
      "eval": {
        "cast": [
          1,
          "varchar"
        ]
      }
    }
  ]
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_optional():
    sql = """select make_interval(1, days => 30)
"""
    json = """{
  "select": [
    {
      "eval": {
        "make_interval": [
          1,
          {
            "optional": {
              "days": 30
            }
          }
        ]
      }
    }
  ]
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_union_all():
    sql = """select
    1 as "id",
    'foo' as "code"
union all
select
    2,
    'boo'
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": 1,
        "alias": "id"
      },
      {
        "eval": "<-foo->",
        "alias": "code"
      }
    ],
    "union all": {
      "select": [
        {
          "eval": 2
        },
        {
          "eval": "<-boo->"
        }
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_from():
    sql = """select
    "id",
    "code"
from "dwh"."bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_select_name():
    sql = """select "id" from "dwh"."bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "name": "id"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_select_name_multi():
    sql = """select
    "id",
    "code",
    "name"
from "dwh"."bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "name": "id"
      },
      {
        "eval": "<=code=>"
      },
      {
        "name": "name"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_from_values():
    sql = """select
    "bar"."id",
    "bar"."code"
from (values (1, 'foo'), (2, 'boo')) as "bar" ("id", "code")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=bar.id=>"
      },
      {
        "eval": "<=bar.code=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "values": [
          [
            1,
            "<-foo->"
          ],
          [
            2,
            "<-boo->"
          ]
        ],
        "columns": [
          "id",
          "code"
        ]
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_limit():
    sql = """select "id" from "bar" limit 5
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "limit": 5
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_offset():
    sql = """select "id" from "bar" offset 5"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "offset": 5
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify(rules=filter_rules(['L025', 'L031', 'L011']))


def test_select_all():
    sql = """select * from "bar"
"""
    json = """{
  "query": {
    "select": [],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_is_distinct_from():
    sql = '''select "code","name","code" is distinct from "name" as "idf" from "bar"'''
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=code=>"
      },
      {
        "eval": "<=name=>"
      },
      {
        "eval": {
          "is distinct from": [
            "<=code=>",
            "<=name=>"
          ]
        },
        "alias": "idf"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.sql


def test_is_not_distinct_from():
    sql = '''select "code","name","code" is not distinct from "name" as "idf" from "bar"'''
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=code=>"
      },
      {
        "eval": "<=name=>"
      },
      {
        "eval": {
          "is not distinct from": [
            "<=code=>",
            "<=name=>"
          ]
        },
        "alias": "idf"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.sql


def test_coalesce():
    sql = """select
    "id",
    coalesce("code", 'no code') as "code"
from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": {
          "coalesce": [
            "<=code=>",
            "<-no code->"
          ]
        },
        "alias": "code"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_coalesce_boolean():
    sql = """select coalesce("code" = "name", false) as "equality" from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {
          "coalesce": [
            {"eq": ["<=code=>", "<=name=>"]},
            false
          ]
        },
        "alias": "equality"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_distinct():
    sql = """select distinct
    "id",
    "code"
from "bar"
"""
    json = """{
  "query": {
    "select distinct": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_distinct_on():
    sql = """select distinct on ("id")
    "id",
    "code"
from "bar"
"""
    json = """{
  "query": {
    "select distinct": [
      {
        "eval": "<=id=>",
        "on": true
      },
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_where():
    sql = """select
    "id",
    "code"
from "dwh"."bar" where "code" = 'foo'
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ],
    "where": {
      "eq": [
        "<=code=>",
        "<-foo->"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_is_null():
    sql = """select
    "id",
    "code"
from "dwh"."bar" where "code" is null
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ],
    "where": {
      "is": [
        "<=code=>",
        null
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_is_not_null():
    sql = """select
    "id",
    "code"
from "dwh"."bar" where "code" is not null
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ],
    "where": {
      "is not": [
        "<=code=>",
        null
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_like():
    sql = """select * from "bar" where "name" like '%oo'
"""
    json = """{
  "query": {
    "select": [],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "like": [
        "<=name=>",
        "<-%oo->"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_not_like():
    sql = """select * from "bar" where "name" not like '%oo'
"""
    json = """{
  "query": {
    "select": [],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "not like": [
        "<=name=>",
        "<-%oo->"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_and():
    sql = """select
    "id",
    "code"
from "dwh"."bar" where ("code" = 'foo' and "id" != 2)
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ],
    "where": {
      "and": [
        {
          "eq": [
            "<=code=>",
            "<-foo->"
          ]
        },
        {
          "neq": [
            "<=id=>",
            2
          ]
        }
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_and_single():
    sql = """select
    "id",
    "code"
from "dwh"."bar" where ("code" = 'foo')
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "dwh.bar"
      }
    ],
    "where": {
      "and": [
        {
          "eq": [
            "<=code=>",
            "<-foo->"
          ]
        }
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_or():
    sql = """select
    "id",
    "code"
from "bar" where ("code" = 'foo' or "code" = 'boo')
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "or": [
        {
          "eq": [
            "<=code=>",
            "<-foo->"
          ]
        },
        {
          "eq": [
            "<=code=>",
            "<-boo->"
          ]
        }
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_parentheses():
    sql = """select "id"
from "bar"
where
    (("code" = 'foo' and "name" = 'boo') or ("code" = 'boo' and "name" = 'foo'))
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "or": [
        {
          "and": [
            {
              "eq": [
                "<=code=>",
                "<-foo->"
              ]
            },
            {
              "eq": [
                "<=name=>",
                "<-boo->"
              ]
            }
          ]
        },
        {
          "and": [
            {
              "eq": [
                "<=code=>",
                "<-boo->"
              ]
            },
            {
              "eq": [
                "<=name=>",
                "<-foo->"
              ]
            }
          ]
        }
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_in():
    sql = """select
    "id",
    "code"
from "bar" where "code" in ('foo', 'boo')
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "in": [
        "<=code=>",
        "<-foo->",
        "<-boo->"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_not_in():
    sql = """select
    "id",
    "code"
from "bar" where "code" not in ('foo', 'boo')
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "not in": [
        "<=code=>",
        "<-foo->",
        "<-boo->"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_in_subquery():
    sql = """select * from "bar" where "code" in (select "code" from "tar")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {
          "value": "*"
        }
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "in": [
        "<=code=>",
        {
          "select": [
            {
              "eval": "<=code=>"
            }
          ],
          "from": [
            {
              "name": "tar"
            }
          ]
        }
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_not_exists():
    sql = """select "id"
from "bar"
where not exists (select * from "tar" where "bar"."code" = "tar"."code")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "not exists": {
        "select": [
          {
            "eval": "*"
          }
        ],
        "from": [
          {
            "name": "tar"
          }
        ],
        "where": {
          "eq": [
            "<=bar.code=>",
            "<=tar.code=>"
          ]
        }
      }
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_between():
    sql = """select * from "bar" where "id" between 1 and 2
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "*"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "between": [
        "<=id=>",
        1,
        2
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_not_between():
    sql = """select * from "bar" where "id" not between 1 and 2
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "*"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "not between": [
        "<=id=>",
        1,
        2
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_group_by():
    sql = """select
    'foo' as "code",
    max("id") as "id"
from "bar" group by "code"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {
          "max": [
            "<=id=>"
          ]
        },
        "alias": "id"
      },
      {
        "eval": "<-foo->",
        "alias": "code"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "group by": [
      "<=code=>"
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_having():
    sql = """select 'foo' as "code" having 'foo' != 'boo'"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<-foo->",
        "alias": "code"
      }
    ],
    "having": {
      "neq": [
        "<-foo->",
        "<-boo->"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_order_by():
    sql = """select 1 as "id" order by "id"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": 1,
        "alias": "id"
      }
    ],
    "order by": [
      "<=id=>"
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_order_by_asc():
    sql = """select 1 as "id" order by "id"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": 1,
        "alias": "id"
      }
    ],
    "order by": [
      {
        "value": "<=id=>"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_order_by_desc_true():
    sql = """select 1 as "id" order by "id" desc
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": 1,
        "alias": "id"
      }
    ],
    "order by": [
      {
        "value": "<=id=>",
        "desc": true
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_order_by_desc_false():
    sql = """select 1 as "id" order by "id"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": 1,
        "alias": "id"
      }
    ],
    "order by": [
      {
        "value": "<=id=>",
        "desc": false
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_order_by_using():
    sql = """select "id" from "bar" order by "id" using >"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "order by": [
      {
        "value": "<=id=>",
        "using": ">"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_implicit_join():
    sql = """select * from "bar", "tar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "*"
      }
    ],
    "from": [
      {
        "name": "bar"
      },
      {
        "name": "tar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_inner_join():
    sql = """select
    "b"."code" as "b_code",
    "t"."code" as "t_code"
from "bar" as "b"
inner join "tar" as "t" on "t"."id" = "b"."id"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=b.code=>",
        "alias": "b_code"
      },
      {
        "eval": "<=t.code=>",
        "alias": "t_code"
      }
    ],
    "from": [
      {
        "name": "bar",
        "alias": "b",
        "inner join": {
          "name": "tar",
          "alias": "t",
          "on": {
            "eq": [
              "<=t.id=>",
              "<=b.id=>"
            ]
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify(rules=filter_rules(['L031']))


def test_join_nested():
    sql = """select
    "b"."code" as "b_code",
    "t"."code" as "t_code"
from "bar" as "b"
inner join "tar" as "t" on "t"."id" = "b"."id"
inner join "car" as "c" on "t"."id" = "c"."id"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=b.code=>",
        "alias": "b_code"
      },
      {
        "eval": "<=t.code=>",
        "alias": "t_code"
      }
    ],
    "from": [
      {
        "name": "bar",
        "alias": "b",
        "inner join": {
          "name": "tar",
          "alias": "t",
          "on": {
            "eq": [
              "<=t.id=>",
              "<=b.id=>"
            ]
          },
          "inner join": {
            "name": "car",
            "alias": "c",
            "on": {
              "eq": [
                "<=t.id=>",
                "<=c.id=>"
              ]
            }
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify(rules=filter_rules(['L031']))


def test_inner_join_and():
    sql = """select
    "bar"."code" as "bar_code",
    "tar"."code" as "tar_code"
from "bar"
inner join "tar" on ("bar"."id" = "tar"."id" and "tar"."code" != 'foo')
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=bar.code=>",
        "alias": "bar_code"
      },
      {
        "eval": "<=tar.code=>",
        "alias": "tar_code"
      }
    ],
    "from": [
      {
        "name": "bar",
        "inner join": {
          "name": "tar",
          "on": {
            "and": [
              {
                "eq": [
                  "<=bar.id=>",
                  "<=tar.id=>"
                ]
              },
              {
                "neq": [
                  "<=tar.code=>",
                  "<-foo->"
                ]
              }
            ]
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_inner_join_on_true():
    sql = """select * from "bar" inner join "tar" on true
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "*"
      }
    ],
    "from": [
      {
        "name": "bar",
        "inner join": {
          "name": "tar",
          "on": {
            "value": true
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_left_join():
    sql = """select
    "bar"."code",
    "tar"."code"
from "bar"
left join "tar" on "bar"."id" = "tar"."id"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=bar.code=>"
      },
      {
        "eval": "<=tar.code=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "left join": {
          "name": "tar",
          "on": {
            "eq": [
              "<=bar.id=>",
              "<=tar.id=>"
            ]
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_full_join_using():
    sql = """select "tar"."name" from "bar" full join "tar" using ("id", "code")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=tar.name=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "full join": {
          "name": "tar",
          "using": [
            "id",
            "code"
          ]
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_nested_full_join():
    sql = """select "tar"."name"
from "bar"
full join "tar" on ("bar"."id" = "tar"."id" and "bar"."code" = "tar"."code")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=tar.name=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "full join": {
          "name": "tar",
          "on": {
            "and": [
              {
                "eq": [
                  "<=bar.id=>",
                  "<=tar.id=>"
                ]
              },
              {
                "eq": [
                  "<=bar.code=>",
                  "<=tar.code=>"
                ]
              }
            ]
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_nested_full_join_using():
    sql = """select "tar"."name"
from "bar"
full join "tar" using ("id", "code")
full join "car" using ("id")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=tar.name=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "full join": {
          "name": "tar",
          "using": [
            "id",
            "code"
          ],
          "full join": {
            "name": "car",
            "using": [
              "id"
            ]
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_cross_join():
    sql = """select "tar"."name" from "bar" cross join "tar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=tar.name=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "cross join": {
          "name": "tar"
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_cross_join_nested():
    sql = """select "tar"."name" from "bar" cross join "tar" cross join "car"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=tar.name=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "cross join": {
          "name": "tar",
          "cross join": {
            "name": "car"
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_from_lateral():
    sql = """select "l"."fault"
from "bar", lateral (select ("total" - "forecast") as "fault") as "l"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=l.fault=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      },
      {
        "lateral": {
          "enclose": {
            "select": [
              {
                "eval": {
                  "sub": [
                    "<=total=>",
                    "<=forecast=>"
                  ]
                },
                "alias": "fault"
              }
            ]
          },
          "alias": "l"
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_join_lateral():
    sql = '''select "lowered" from "bar" inner join lateral lower("code") as "lowered" on "code" = "lowered"'''
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=lowered=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "inner join": {
          "alias": "lowered",
          "on": {
            "eq": [
              "<=code=>",
              "<=lowered=>"
            ]
          },
          "lateral": {
            "lower": [
              "<=code=>"
            ]
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.sql


def test_subquery():
    sql = """select
    "t"."id",
    "t"."code"
from (
    select
        1 as "id",
        'foo' as "code"
) as "t"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=t.id=>"
      },
      {
        "eval": "<=t.code=>"
      }
    ],
    "from": [
      {
        "alias": "t",
        "enclose": {
          "select": [
            {
              "eval": 1,
              "alias": "id"
            },
            {
              "eval": "<-foo->",
              "alias": "code"
            }
          ]
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify(rules=filter_rules(['L003']))


def test_subquery_tamed():
    sql = """select
    "t"."id",
    "t"."code"
from (select
    1 as "id",
    'foo' as "code"
from "bar") as "t"
where "code" = 'foo'
order by "code"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=t.id=>"
      },
      {
        "eval": "<=t.code=>"
      }
    ],
    "from": [
      {
        "alias": "t",
        "enclose": {
          "select": [
            {
              "eval": 1,
              "alias": "id"
            },
            {
              "eval": "<-foo->",
              "alias": "code"
            }
          ],
          "from": [
            {
              "name": "bar"
            }
          ]
        }
      }
    ],
    "where": {
      "eq": [
        "<=code=>",
        "<-foo->"
      ]
    },
    "order by": [
      "<=code=>"
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify(rules=filter_rules(['L003']))


def test_case():
    sql = """select
    case
        when "code" = 'foo' then 1 when "code" = 'boo' then 2 else 3
    end as "ordering"
from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {
          "case": {
            "conditions": [
              {
                "when": {
                  "eq": [
                    "<=code=>",
                    "<-foo->"
                  ]
                },
                "then": {
                  "value": 1
                }
              },
              {
                "when": {
                  "eq": [
                    "<=code=>",
                    "<-boo->"
                  ]
                },
                "then": {
                  "value": 2
                }
              }
            ],
            "else": {
              "value": 3
            },
            "alias": "ordering"
          }
        }
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_case_simple():
    sql = """select case "code" when 'foo' then 1 when 'boo' then 2 else 3 end as "ordering"
from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {
          "case": {
            "expression": {
                  "value": "<=code=>"
                },
            "conditions": [
              {
                "when": {
                  "value": "<-foo->"
                },
                "then": {
                  "value": 1
                }
              },
              {
                "when": {
                  "value": "<-boo->"
                },
                "then": {
                  "value": 2
                }
              }
            ],
            "else": {
              "value": 3
            },
            "alias": "ordering"
          }
        }
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_with():
    sql = """with "t" as (
    select
        1 as "id",
        'foo' as "code"
)

select
    "id",
    "code"
from "t"
"""
    json = """{
  "query": {
    "with": {
      "t": {
        "select": [
          {
            "eval": 1,
            "alias": "id"
          },
          {
            "eval": "<-foo->",
            "alias": "code"
          }
        ]
      }
    },
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": "<=code=>"
      }
    ],
    "from": [
      {
        "name": "t"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_with_recursive():
    sql = """with recursive "tree" as (select
    "code",
    "parent"
from "bar" where "code" = "parent" union distinct select
    "bar"."code",
    "bar"."parent"
from "bar" inner join "tree" on "bar"."code" = "tree"."parent")

select *
from "tree"
"""
    json = """{
  "query": {
    "with recursive": {
      "tree": {
        "select": [
          {
            "eval": "<=code=>"
          },
          {
            "eval": "<=parent=>"
          }
        ],
        "from": [
          {
            "name": "bar"
          }
        ],
        "where": {
          "eq": [
            "<=code=>",
            "<=parent=>"
          ]
        },
        "union distinct": {
          "select": [
            {
              "eval": "<=bar.code=>"
            },
            {
              "eval": "<=bar.parent=>"
            }
          ],
          "from": [
            {
              "name": "bar",
              "inner join": {
                "name": "tree",
                "on": {
                  "eq": [
                    "<=bar.code=>",
                    "<=tree.parent=>"
                  ]
                }
              }
            }
          ]
        }
      }
    },
    "select": [
      {
        "eval": "*"
      }
    ],
    "from": [
      {
        "name": "tree"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify(rules=filter_rules(['L003']))


def test_with_multiple():
    sql = """with "bar" as (select
    "id",
    "code"
from "bar" where "code" != 'foo'),

"tar" as (select
    "id",
    "code"
from "tar" where "code" != 'boo')

select
    "bar"."id",
    "tar"."code"
from "bar" inner join "tar" on "bar"."id" = "tar"."id"
"""
    json = """{
  "query": {
    "with": {
      "bar": {
        "select": [
          {
            "eval": "<=id=>"
          },
          {
            "eval": "<=code=>"
          }
        ],
        "from": [
          {
            "name": "bar"
          }
        ],
        "where": {
          "neq": [
            "<=code=>",
            "<-foo->"
          ]
        }
      },
      "tar": {
        "select": [
          {
            "eval": "<=id=>"
          },
          {
            "eval": "<=code=>"
          }
        ],
        "from": [
          {
            "name": "tar"
          }
        ],
        "where": {
          "neq": [
            "<=code=>",
            "<-boo->"
          ]
        }
      }
    },
    "select": [
      {
        "eval": "<=bar.id=>"
      },
      {
        "eval": "<=tar.code=>"
      }
    ],
    "from": [
      {
        "name": "bar",
        "inner join": {
          "name": "tar",
          "on": {
            "eq": [
              "<=bar.id=>",
              "<=tar.id=>"
            ]
          }
        }
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_cube():
    sql = """select
    "code",
    count("id")
from "bar" group by cube("code")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=code=>"
      },
      {
        "eval": {
          "count": [
            "<=id=>"
          ]
        }
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "group by": [
      {
        "cube": [
          "<=code=>"
        ]
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_rollup():
    sql = """select
    "code",
    "name",
    count("id")
from "bar" group by rollup("code", "name")
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=code=>"
      },
      
      {
        "eval": "<=name=>"
      },
      {
        "eval": {
          "count": [
            "<=id=>"
          ]
        }
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "group by": [
      {
        "rollup": [
          "<=code=>",
          "<=name=>"
        ]
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_grouping_sets():
    sql = """select
    "code",
    "name",
    grouping("name"),
    sum("amount")
from "bar"
group by grouping sets (("code", "name"), ("name"), ())
having grouping("name") = 0
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=code=>"
      },
      {
        "eval": "<=name=>"
      },
      {
        "eval": {
          "grouping": [
            "<=name=>"
          ]
        }
      },
      {
        "eval": {
          "sum": [
            "<=amount=>"
          ]
        }
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "group by": [
      {
        "grouping sets": [
          {
            "enclose": [
              "<=code=>",
              "<=name=>"
            ]
          },
          {
            "enclose": [
              "<=name=>"
            ]
          },
          {
            "enclose": []
          }
        ]
      }
    ],
    "having": {
      "eq": [
        {
          "grouping": [
            "<=name=>"
          ]
        },
        0
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_window():
    sql = """select
    "id",
    last_value("code") over (partition by "id" order by "name") as "last"
from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      },
      {
        "eval": {
          "last_value": [
            "<=code=>"
          ],
          "over": {
            "partition by": [
              "<=id=>"
            ],
            "order by": [
              "<=name=>"
            ]
          }
        },
        "alias": "last"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_nested():
    sql = """select coalesce(min("code") = "name", false) as "equality"
from "bar"
group by "name"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {
          "coalesce": [
            {
              "eq": [
                {
                  "min": [
                    "<=code=>"
                  ]
                },
                "<=name=>"
              ]
            },
            false
          ]
        },
        "alias": "equality"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "group by": [
      "<=name=>"
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_insert():
    sql = """insert into "bar" ("id", "code") select
    1,
    'foo'
"""
    json = """{
  "query": {
    "insert": {
      "name": "bar",
      "columns": [
        "id",
        "code"
      ],
     "select": [
       {
         "eval": 1
       },
       {
         "eval": "<-foo->"
       }
     ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_insert_values():
    sql = """insert into "bar" ("id", "code") values (1, 'foo'), (2, 'boo')
"""
    json = """{
  "query": {
    "insert": {
      "name": "bar",
      "columns": [
        "id",
        "code"
      ],
      "values": [
        [
          1,
          "<-foo->"
        ],
        [
          2,
          "<-boo->"
        ]
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_update():
    sql = """update "bar" set "code" = 'boo' where "id" = 1
"""
    json = """{
  "query": {
    "update": {
      "name": "bar",
      "set": [
        {
          "name": "code",
          "eval": "<-boo->"
        }
      ]
    },
    "where": {
      "eq": [
        "<=id=>",
        1
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_update_subquery():
    sql = """update "bar" set "code" = 'boo'
from (select "code" from "tar") as "subquery"
where "bar"."code" = "subquery"."code"
"""
    json = """{
  "query": {
    "update": {
      "name": "bar",
      "set": [
        {
          "name": "code",
          "eval": "<-boo->"
        }
      ]
    },
    "from": [
      {
        "alias": "subquery",
        "enclose": {
          "select": [
            {
              "eval": "<=code=>"
            }
          ],
          "from": [
            {
              "name": "tar"
            }
          ]
        }
      }
    ],
    "where": {
      "eq": [
        "<=bar.code=>",
        "<=subquery.code=>"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_update_values():
    sql = """update "bar" set "code" = "t"."code"
from (values (1, 'foo')) as "t" ("id", "code")
where "bar"."id" = "t"."id"
"""
    json = """{
  "query": {
    "update": {
      "name": "bar",
      "set": [
        {
          "name": "code",
          "eval": "<=t.code=>"
        }
      ]
    },
    "from": [
      {
        "name": "t",
        "values": [
          [
            1,
            "<-foo->"
          ]
        ],
        "columns": [
          "id",
          "code"
        ]
      }
    ],
    "where": {
      "eq": [
        "<=bar.id=>",
        "<=t.id=>"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_delete():
    sql = """delete from "bar" where "code" = 'foo'
"""
    json = """{
  "query": {
    "delete": {
      "name": "bar"
    },
    "where": {
      "eq": [
        "<=code=>",
        "<-foo->"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_delete_subquery():
    sql = """delete from "bar" where "code" in (select "code" from "tar")
"""
    json = """{
  "query": {
    "delete": {
      "name": "bar"
    },
    "where": {
      "in": [
        "<=code=>",
        {
          "select": [
            {
              "eval": "<=code=>"
            }
          ],
          "from": [
            {
              "name": "tar"
            }
          ]
        }
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_delete_values():
    sql = """delete from "bar" using (values (1, 'foo')) as "t" ("id", "code")
where "bar"."id" = "t"."id"
"""
    json = """{
  "query": {
    "delete": {
      "name": "bar",
      "using": [
        {
          "name": "t",
          "values": [
            [
              1,
              "<-foo->"
            ]
          ],
          "columns": [
            "id",
            "code"
          ]
        }
      ]
    },
    "where": {
      "eq": [
        "<=bar.id=>",
        "<=t.id=>"
      ]
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_on_conflict():
    sql = """insert into "bar" ("id", "code") values (1, 'foo') on conflict (
    "id", "code"
) do update set "code"
= 'boo'
"""
    json = """{
  "query": {
    "insert": {
      "name": "bar",
      "columns": [
        "id",
        "code"
      ],
      "values": [
        [
          1,
          "<-foo->"
        ]
      ]
    },
    "on conflict": {
      "columns": [
        "id",
        "code"
      ],
      "update": {
        "set": [
          {
            "name": "code",
            "eval": "<-boo->"
          }
        ]
      }
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_on_constraint():
    sql = """insert into "bar" ("id", "code") values (
    1, 'foo'
) on conflict on constraint "bar_pkey" do nothing
"""
    json = """{
  "query": {
    "insert": {
      "name": "bar",
      "columns": [
        "id",
        "code"
      ],
      "values": [
        [
          1,
          "<-foo->"
        ]
      ]
    },
    "on conflict": {
      "constraint": "<=bar_pkey=>"
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_update_with():
    sql = """with "t" as (update "bar" set "amount" = ("amount" * 2) returning *)

select *
from "t"
"""
    json = """{
  "query": {
    "with": {
      "t": {
        "update": {
          "name": "bar",
          "set": [
            {
              "name": "amount",
              "eval": {
                "mult": [
                  "<=amount=>",
                  2
                ]
              }
            }
          ]
        },
        "returning": [
          "*"
        ]
      }
    },
    "select": [
      {
        "eval": "*"
      }
    ],
    "from": [
      {
        "name": "t"
      }
    ]
  }
}
"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_upsert():
    sql = ("""with "dataset"("id","code") as (select t.* from (values ( 1,'foo'),( 2,'boo')) "t"), \
"upsert" as (update "bar" b set "id" = "d"."id","code" = "d"."code" \
from "dataset" as "d" \
where "b"."id" = "d"."id" returning "b".*) \
insert into "bar"("id","code") select "id","code" \
from "dataset" \
where not exists (select * from "upsert" as "up" where "up"."id" = "dataset"."id")""")
    json = """{
  "query": {
    "with": {
      "dataset": {
        "columns": [
          "id",
          "code"
        ],
        "select": [
          {
            "eval": "t.*"
          }
        ],
        "from": [
          {
            "name": "t",
            "values": [
              [
                1,
                "<-foo->"
              ],
              [
                2,
                "<-boo->"
              ]
            ]
          }
        ]
      },
      "upsert": {
        "update": {
          "name": "bar",
          "alias": "b",
          "set": [
            {
              "name": "id",
              "eval": "<=d.id=>"
            },
            {
              "name": "code",
              "eval": "<=d.code=>"
            }
          ]
        },
        "from": [
          {
            "name": "dataset",
            "alias": "d"
          }
        ],
        "where": {
          "eq": [
            "<=b.id=>",
            "<=d.id=>"
          ]
        },
        "returning": [
          "<=b=>.*"
        ]
      }
    },
    "insert": {
      "name": "bar",
      "columns": [
        "id",
        "code"
      ],
      "select": [
        {
          "eval": "<=id=>"
        },
        {
          "eval": "<=code=>"
        }
      ],
      "from": [
        {
          "name": "dataset"
        }
      ],
      "where": {
        "not exists": {
          "select": [
            {
              "eval": "*"
            }
          ],
          "from": [
            {
              "name": "upsert",
              "alias": "up"
            }
          ],
          "where": {
            "eq": [
              "<=up.id=>",
              "<=dataset.id=>"
            ]
          }
        }
      }
    }
  }
}
"""
    query = Query(raw=json)
    assert sql == query.sql


def test_custom_builder():

    class CustomBuilder(PGBuilder):
        def _neq(self, value: List[SCBND_T]) -> str:
            return self._commute_(value, op='<>')

    sql = """select "id" from "bar" where "code" <> 'foo'
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "neq": [
        "<=code=>",
        "<-foo->"
      ]
    }
  }
}
"""
    query = Query(raw=json, builder=CustomBuilder())
    assert sql == query.prettify()


def test_select_variable():
    sql = """select 'b'
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<-<+a+>->"
      }
    ]
  }
}
"""
    query = Query(raw=json, constants={"a": "b"})
    assert sql == query.prettify()


def test_select_numeric_variable():
    sql = """select 1
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<+a+>"
      }
    ]
  }
}
"""
    query = Query(raw=json, constants={"a": 1})
    assert sql == query.prettify()


def test_select_name_variable():
    sql = """select "id" from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=<+a+>=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json, constants={"a": "id"})
    assert sql == query.prettify()


def test_select_binding():
    sql = """select 'b'
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<-{{ a }}->"
      }
    ]
  }
}
"""
    query = Query(raw=json, bindings={"a": "b"})
    assert sql == query.prettify()


def test_select_numeric_binding():
    sql = """select 1
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {{ 1 }}
      }
    ]
  }
}
"""
    query = Query(raw=json, bindings={"a": 1})
    assert sql == query.prettify()


def test_select_name_binding():
    sql = """select "id" from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<={{ a }}=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json, bindings={"a": "id"})
    assert sql == query.prettify()


def test_select_binding_variable():
    sql = """select
    'b',
    'c'
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<-{{ a }}->"
      },
      {
        "eval": "<-<+a+>->"
      }
    ]
  }
}
"""
    query = Query(raw=json, bindings={"a": "b"}, constants={"a": "c"})
    assert sql == query.prettify()


def test_select_numeric_binding_variable():
    sql = """select
    1,
    2
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": {{ 1 }}
      },
      {
        "eval": "<+a+>"
      }
    ]
  }
}
"""
    query = Query(raw=json, bindings={"a": 1}, constants={"a": 2})
    assert sql == query.prettify()


def test_select_name_binding_variable():
    sql = """select
    "id",
    "name"
from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<={{ a }}=>"
      },
      {
        "eval": "<=<+a+>=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}
"""
    query = Query(raw=json, bindings={"a": "id"}, constants={"a": "name"})
    assert sql == query.prettify()


def test_bitwise_and():
    sql = """select "id" & 1 as "bit_one" from "bar"
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=> & 1",
        "alias": "bit_one"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ]
  }
}"""
    query = Query(raw=json)
    assert sql == query.prettify()


def test_bitwise_and_in_where():
    sql = """select "id" from "bar" where "id" & 1 = 1
"""
    json = """{
  "query": {
    "select": [
      {
        "eval": "<=id=>"
      }
    ],
    "from": [
      {
        "name": "bar"
      }
    ],
    "where": {
      "eq": [
        "<=id=> & 1",
        1
      ]
    }
  }
}"""
    query = Query(raw=json)
    assert sql == query.prettify()
