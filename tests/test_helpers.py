""" This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

Created on Aug 14, 2021

@author: pymancer@gmail.com (polyanalitika.ru)
"""
import json

from box import Box
from jsqlib.engine import Query
from jsqlib.helpers.common import Tokenizer

CONSTANTS = {
    'a': 'b',
    'c': '<==>',
    'd': 'ef',
    "g'h'": 'i',
    'j': 'k+>>l',
    'm': 'n<==>o',
    'p': 2,
    'q': True,
    'r': False,
    's': None,
    't': '',
    'u': 'true',
    'v': 'false',
    'w': 'null',
    'x': '''first line
second line
        third line with indent
    
    last line'''
}
TRANSLATOR = Box({
    "cast": {
        "true": "True",
        "false": "False",
        "null": "None"
    },
    "quote": {
        'double': {
            "left": '"',
            "right": '"',
            "delimiter": None
        },
        'single': {
            "left": "'",
            "right": "'",
            "delimiter": None
        }
    }
}, frozen_box=True)

class TestTokenizer:
    def test_dquote(self):
        tokenizer = Tokenizer()
        assert tokenizer.dquote('') == '""'
        assert tokenizer.dquote('bar') == '"bar"'
        assert tokenizer.dquote('bar.foo') == '"bar"."foo"'
        assert tokenizer.dquote('bar.foo.boo') == '"bar"."foo"."boo"'

    def test_dquote_custom_translator(self):
        tokenizer = Tokenizer(translator=TRANSLATOR)
        assert tokenizer.dquote('') == '""'
        assert tokenizer.dquote('bar') == '"bar"'
        assert tokenizer.dquote('bar.foo') == '"bar.foo"'
        assert tokenizer.dquote('bar.foo.boo') == '"bar.foo.boo"'

    def test_stringify(self):
        tokenizer = Tokenizer(constants=CONSTANTS)
        assert tokenizer.stringify(0) == '0'
        assert tokenizer.stringify(1) == '1'
        assert tokenizer.stringify(True) == 'true'
        assert tokenizer.stringify(False) == 'false'
        assert tokenizer.stringify(None) == 'null'
        assert tokenizer.stringify('') == ''
        assert tokenizer.stringify('a') == 'a'
        assert tokenizer.stringify('abcd') == 'abcd'
        assert tokenizer.stringify('<=a') == '<=a'
        assert tokenizer.stringify('a<=a') == 'a<=a'
        assert tokenizer.stringify('<=') == '<='
        assert tokenizer.stringify('=>') == '=>'
        assert tokenizer.stringify('<=<=') == '<=<='
        assert tokenizer.stringify('=>=>') == '=>=>'
        assert tokenizer.stringify('=><=') == '=><='
        assert tokenizer.stringify('"<=') == '"<='
        assert tokenizer.stringify('""') == '""'
        assert tokenizer.stringify('<=a=>') == '"a"'
        assert tokenizer.stringify('<=ab=>') == '"ab"'
        assert tokenizer.stringify('<="=>') == '"""'
        assert tokenizer.stringify('<=""=>') == '""""'
        assert tokenizer.stringify("<=''=>") == '''"''"'''
        assert tokenizer.stringify('<=abc=>') == '"abc"'
        assert tokenizer.stringify('<=a.bc=>') == '"a"."bc"'
        assert tokenizer.stringify('<=a.b.c=>') == '"a"."b"."c"'
        assert tokenizer.stringify('<=<==>') == '<=""'
        assert tokenizer.stringify('<==>=>') == '""=>'
        assert tokenizer.stringify('<=<==>=>') == '""""'
        assert tokenizer.stringify('a<==>a') == 'a""a'
        assert tokenizer.stringify('a<-<==>->a') == """a'""'a"""
        assert tokenizer.stringify('<-<==>->') == """'""'"""
        assert tokenizer.stringify('a<=b=>a') == 'a"b"a'
        assert tokenizer.stringify('a<-b->a') == "a'b'a"
        assert tokenizer.stringify('<-a<-b->a->') == "'a'b'a'"
        assert tokenizer.stringify('<=a<=b<=c=>b=>a=>') == '"a"b"c"b"a"'
        assert tokenizer.stringify('<-<=a<=b<=c=>b=>a=>->') == """'"a"b"c"b"a"'"""
        assert tokenizer.stringify('a<=b=><=d=>a') == 'a"b""d"a'
        assert tokenizer.stringify('a<=b=>c<=d=>a') == 'a"b"c"d"a'
        assert tokenizer.stringify('a<=b=><-c-><=d=>a') == """a"b"'c'"d"a"""
        assert tokenizer.stringify('<-a<=b=><-c-><=d=>a->') == """'a"b"'c'"d"a'"""
        assert tokenizer.stringify('<<==>>') == '<==>'
        assert tokenizer.stringify('<<=abc=>>') == '<=abc=>'
        assert tokenizer.stringify('<<-abc->>') == '<-abc->'
        assert tokenizer.stringify('ab=>>c') == 'ab=>c'
        assert tokenizer.stringify('<<+abc') == '<+abc'
        assert tokenizer.stringify('d<<+abc') == 'd<+abc'
        assert tokenizer.stringify('ab=>>>c') == 'ab=>>c'
        assert tokenizer.stringify('<<<+abc') == '<<+abc'
        assert tokenizer.stringify('d<<<+abc') == 'd<<+abc'
        assert tokenizer.stringify('<=<<=abc=>>=>') == '"<=abc=>"'
        assert tokenizer.stringify('d<=<<=abc=>>=>d') == 'd"<=abc=>"d'
        assert tokenizer.stringify('<=<=a=>>=>') == '<="a=>"'
        assert tokenizer.stringify('<=<<=a=>=>') == '"<=a"=>'
        assert tokenizer.stringify('<=<=a=>>>=>') == '<="a=>>"'
        assert tokenizer.stringify('<=<<<=a=>=>') == '"<<=a"=>'
        assert tokenizer.stringify('<-d<=<<=abc=>>=>d->') == """'d"<=abc=>"d'"""
        assert tokenizer.stringify('<=a<=b<=c=>b<=c<=d=>c=>b=>a=>') == '"a"b"c"b"c"d"c"b"a"'
        assert tokenizer.stringify('<=a<=b<=c=>b<=c<=<<=d=>>=>c=>b=>a=>') == '"a"b"c"b"c"<=d=>"c"b"a"'
        assert tokenizer.stringify('<= <=b=> =>') == '" "b" "'
        assert tokenizer.stringify('<=<-=>=>') == '"<-"=>'
        assert tokenizer.stringify('<=<<-=>->') == '"<-"->'
        assert tokenizer.stringify('<=<-=>->') == '"<-"->'
        assert tokenizer.stringify('<-<=<-=>->') == """'"<-"'"""
        assert tokenizer.stringify('<-<=<-=>->->') == """'"<-"'->"""
        assert tokenizer.stringify('<-<-=>->') == "<-'=>'"
        assert tokenizer.stringify('<=<-->') == "<=''"
        assert tokenizer.stringify('<-abc->') == "'abc'"
        assert tokenizer.stringify('<+a+>') == 'b'
        assert tokenizer.stringify('<+q+>') == 'true'
        assert tokenizer.stringify('<+r+>') == 'false'
        assert tokenizer.stringify('<+s+>') == 'null'
        assert tokenizer.stringify('<+t+>') == ''
        assert tokenizer.stringify('<+u+>') == 'true'
        assert tokenizer.stringify('<+v+>') == 'false'
        assert tokenizer.stringify('<+w+>') == 'null'
        assert tokenizer.stringify('a<+r+>b') == 'afalseb'
        assert tokenizer.stringify('<-<+a+>->') == "'b'"
        assert tokenizer.stringify('<=<+a+>=>') == '"b"'
        assert tokenizer.stringify('<-<=<+a+>=>->') == """'"b"'"""
        assert tokenizer.stringify('<+b+>') == '<+b+>'
        assert tokenizer.stringify('<-<+b+>->') == "'<+b+>'"
        assert tokenizer.stringify('<+c+>') == '<==>'
        assert tokenizer.stringify('<=<+c+>=>') == '"<==>"'
        assert tokenizer.stringify('<+m+>') == 'n<==>o'
        assert tokenizer.stringify('<=<+d+>=>') == '"ef"'
        assert tokenizer.stringify("<+g'h'+>") == 'i'
        assert tokenizer.stringify('<+g<-h->+>') == 'i'
        assert tokenizer.stringify('<+j+>') == 'k+>>l'
        assert tokenizer.stringify('<=<+j+>=>') == '"k+>>l"'
        assert tokenizer.stringify('<-*->') == "'*'"
        assert tokenizer.stringify('<= * =>') == '" * "'
        assert tokenizer.stringify('<= * =>') == '" * "'
        assert tokenizer.stringify('<= <-*-> =>') == '''" '*' "'''
        assert tokenizer.stringify('<=public=>.foo') == '"public".foo'
        assert tokenizer.stringify('"<~3~>"') == '3'
        assert tokenizer.stringify('"<<~3~>>"') == '"<~3~>"'
        assert tokenizer.stringify('"<<<~3~>>>"') == '"<<~3~>>"'
        assert tokenizer.stringify('a"<<~3~>>"b') == 'a"<~3~>"b'
        assert tokenizer.stringify('<~3~>') == '<~3~>'
        assert tokenizer.stringify("'<~true~>'") == "true"
        assert tokenizer.stringify("'<~<+q+>~>'") == "true"
        assert tokenizer.stringify('<-0<=1"<~3~>"2=>4->') == """'0"132"4'"""
        assert tokenizer.stringify('''{"a": 1, "b": "<~<+p+>~>", "c": 3}''') == '''{"a": 1, "b": 2, "c": 3}'''
        tokenizer = Tokenizer()
        assert tokenizer.stringify('<+a+>') == '<+a+>'
        assert tokenizer.stringify('<=<+a+>=>') == '"<+a+>"'


    def test_stringify_multiline(self):
        tokenizer = Tokenizer(constants=CONSTANTS)
        assert tokenizer.stringify('\n') == '\n'
        assert tokenizer.stringify('ab\ncd') == 'ab\ncd'
        assert tokenizer.stringify('\r\n') == '\r\n'
        assert tokenizer.stringify('ab\r\ncd') == 'ab\r\ncd'
        assert tokenizer.stringify('''
''') == '''
'''
        assert tokenizer.stringify('''ab
cd''') == '''ab
cd'''
        assert tokenizer.stringify('''<=a
''') == '''<=a
'''
        assert tokenizer.stringify('''a
<=a''') == '''a
<=a'''
        assert tokenizer.stringify('''<
=''') == '''<
='''
        assert tokenizer.stringify('''
=>
''') == '''
=>
'''
        assert tokenizer.stringify('''<=
<=''') == '''<=
<='''
        assert tokenizer.stringify('''=>
=>''') == '''=>
=>'''
        assert tokenizer.stringify('''=>
<=''') == '''=>
<='''
        assert tokenizer.stringify('''
"<=''') == '''
"<='''
        assert tokenizer.stringify('''"
"''') == '''"
"'''
        assert tokenizer.stringify('''
<=a=>
''') == '''
"a"
'''
        assert tokenizer.stringify('''<=
a
=>''') == '''"
a
"'''
        assert tokenizer.stringify('''<=

a
=>''') == '''"

a
"'''
        assert tokenizer.stringify('''<=

a

=>''') == '''"

a

"'''
        assert tokenizer.stringify('''<=

a

=>
''') == '''"

a

"
'''
        assert tokenizer.stringify('''<
=a=>''') == '''<
=a=>'''
        assert tokenizer.stringify('''<=a=
>''') == '''<=a=
>'''
        assert tokenizer.stringify('''<
=a=
>''') == '''<
=a=
>'''
        assert tokenizer.stringify('''<=
ab
=>''') == '''"
ab
"'''
        assert tokenizer.stringify('''<=a
b=>''') == '''"a
b"'''
        assert tokenizer.stringify('''<=
"=>''') == '''"
""'''
        assert tokenizer.stringify('''<=
        "=>''') == '''"
        ""'''
        assert tokenizer.stringify('''<=
"
=>''') == '''"
"
"'''
        assert tokenizer.stringify('''<="
"=>''') == '''""
""'''
        assert tokenizer.stringify("""
<=''=>
""") == '''
"''"
'''
        assert tokenizer.stringify("""<='
'=>""") == '''"'
'"'''
        assert tokenizer.stringify('''
<=abc=>
''') == '''
"abc"
'''
        assert tokenizer.stringify('''<=a
.bc=>''') == '''"a
"."bc"'''
        assert tokenizer.stringify('''<=a.
bc=>''') == '''"a"."
bc"'''
        assert tokenizer.stringify('''<=a
.
bc=>''') == '''"a
"."
bc"'''
        assert tokenizer.stringify('''<=a
        .bc=>''') == '''"a
        "."bc"'''
        assert tokenizer.stringify('''<=a.bc
        =>''') == '''"a"."bc
        "'''
        assert tokenizer.stringify('''<
        =a.bc=>''') == '''<
        =a.bc=>'''
        assert tokenizer.stringify('''<=a.
        b.c=>''') == '''"a"."
        b"."c"'''
        assert tokenizer.stringify('''<=a.b
        .c=>''') == '''"a"."b
        "."c"'''
        assert tokenizer.stringify('''<=a.b.c
        =>''') == '''"a"."b"."c
        "'''
        assert tokenizer.stringify('''<=<=
=>''') == '''<="
"'''
        assert tokenizer.stringify('''<=
        <==>''') == '''<=
        ""'''
        assert tokenizer.stringify('''<=
        =>=>''') == '''"
        "=>'''
        assert tokenizer.stringify('''<==>
        =>''') == '''""
        =>'''
        assert tokenizer.stringify('''<=<=
        =>=>''') == '''""
        ""'''
        assert tokenizer.stringify('''<=
        <==>
        =>''') == '''"
        ""
        "'''
        assert tokenizer.stringify('''a<=
        =>a''') == '''a"
        "a'''
        assert tokenizer.stringify('''a
        <==>
        a''') == '''a
        ""
        a'''
        assert tokenizer.stringify('''a<-
        <==>
        ->a''') == """a'
        ""
        'a"""
        assert tokenizer.stringify('''a
        <-<=

        =>->a''') == """a
        '"

        "'a"""
        assert tokenizer.stringify('''<-<=
        =>->''') == """'"
        "'"""
        assert tokenizer.stringify('''<-<=

        =>->''') == """'"

        "'"""
        assert tokenizer.stringify('''a
        <=b=>a''') == '''a
        "b"a'''
        assert tokenizer.stringify('''a
        <=b=>
        a''') == '''a
        "b"
        a'''
        assert tokenizer.stringify('''a
        <-b

        ->a''') == '''a
        'b

        'a'''
        assert tokenizer.stringify('''a<-b
        ->a''') == '''a'b
        'a'''
        assert tokenizer.stringify('''
        <-a<-b->a->''') == """
        'a'b'a'"""
        assert tokenizer.stringify('''<-

        a<-b
        ->a->''') == """'

        a'b
        'a'"""
        assert tokenizer.stringify('''<-

        a<-b
        ->a->''') == """'

        a'b
        'a'"""
        assert tokenizer.stringify('''<=a
        <=b
        <=c=>
        b=>a=>''') == '''"a
        "b
        "c"
        b"a"'''
        assert tokenizer.stringify('''<=a<=b<=c=>

        b=>a=>''') == '''"a"b"c"

        b"a"'''
        assert tokenizer.stringify('''<-
        <=a
        <=b

        <=c=>
        b=>a=>->''') == """'
        "a
        "b

        "c"
        b"a"'"""
        assert tokenizer.stringify('''<-<=a<=b
        <=c
        =>b=>a=>->''') == """'"a"b
        "c
        "b"a"'"""
        assert tokenizer.stringify('''a<=
        b=><
        =d=>a''') == '''a"
        b"<
        =d=>a'''
        assert tokenizer.stringify('''a<=b=
        ><
        =d=>a''') == '''a"b=
        ><
        =d"a'''
        assert tokenizer.stringify('''a<=b=>
        c
        <=d=>a''') == '''a"b"
        c
        "d"a'''
        assert tokenizer.stringify('''a<=

        b=>c<=d

        =>a''') == '''a"

        b"c"d

        "a'''
        assert tokenizer.stringify('''a<=b

        =><-c-><=
        d=>a''') == """a"b

        "'c'"
        d"a"""
        assert tokenizer.stringify('''a<=b
=><-c-><=
d=>a''') == """a"b
"'c'"
d"a"""
        assert tokenizer.stringify('''<-a<=b
=><-
        c-><=d=>a->''') == """'a"b
"'
        c'"d"a'"""
        assert tokenizer.stringify('''<-a<=b=>
        <-
c-><=d=>a
        ->''') == """'a"b"
        '
c'"d"a
        '"""
        assert tokenizer.stringify('''<<=
=>>''') == '''<=
=>'''
        assert tokenizer.stringify('''<
<==>>''') == '''<
<==>'''
        assert tokenizer.stringify('''<<=a

        bc=>>''') == '''<=a

        bc=>'''
        assert tokenizer.stringify('''<<=abc=>
        >''') == '''<=abc=>
        >'''
        assert tokenizer.stringify('''<<-
abc->>''') == '''<-
abc->'''
        assert tokenizer.stringify('''<<-ab
        c->''') == """<-ab
        c->"""
        assert tokenizer.stringify('''ab
        =>>c''') == '''ab
        =>c'''
        assert tokenizer.stringify('''ab=>
        >c''') == '''ab=>
        >c'''
        assert tokenizer.stringify('''<<
        +abc''') == '''<<
        +abc'''
        assert tokenizer.stringify('''
        <<+
        abc''') == '''
        <+
        abc'''
        assert tokenizer.stringify('''d<
        <+abc''') == '''d<
        <+abc'''
        assert tokenizer.stringify('''d
        <<+abc''') == '''d
        <+abc'''
        assert tokenizer.stringify('''ab=>>
        >c''') == '''ab=>
        >c'''
        assert tokenizer.stringify('''ab=>
        >>c''') == '''ab=>
        >>c'''
        assert tokenizer.stringify('''<
        <<+abc''') == '''<
        <+abc'''
        assert tokenizer.stringify('''<<
        <+abc''') == '''<<
        <+abc'''
        assert tokenizer.stringify('''d
        <<<+abc''') == '''d
        <<+abc'''
        assert tokenizer.stringify('''d<
        <
        <+abc''') == '''d<
        <
        <+abc'''
        assert tokenizer.stringify('''<=<<=
        abc=>>=>''') == '''"<=
        abc=>"'''
        assert tokenizer.stringify('''<=<
        <=abc=>
        >=>''') == '''"<
        "abc"
        >"'''
        assert tokenizer.stringify('''d<=<<=abc

        =>>=>d''') == '''d"<=abc

        =>"d'''
        assert tokenizer.stringify('''d<=<<=abc=>
        >=>d''') == '''d"<=abc"
        >=>d'''
        assert tokenizer.stringify('''<=<=a
        =>>=>''') == '''<="a
        =>"'''
        assert tokenizer.stringify('''<=<=a=>
        >=>''') == '''""a"
        >"'''
        assert tokenizer.stringify('''<=
        <<=a=>=>''') == '''"
        <=a"=>'''
        assert tokenizer.stringify('''<=<
        <=a=>=>''') == '''"<
        "a""'''
        assert tokenizer.stringify('''<=<=a=>>>
        =>''') == '''<="a=>>
        "'''
        assert tokenizer.stringify('''<=<=a
        =>>>=>''') == '''<="a
        =>>"'''
        assert tokenizer.stringify('''<=<=a=>>
        >=>''') == '''<="a=>
        >"'''
        assert tokenizer.stringify('''<=
        <<<=a=>=>''') == '''"
        <<=a"=>'''
        assert tokenizer.stringify('''<=<<<=
        a=>=>''') == '''"<<=
        a"=>'''
        assert tokenizer.stringify('''<=<
        <<=a=>=>''') == '''"<
        <=a"=>'''
        assert tokenizer.stringify('''<-d<=
        <<=abc=>>
        =>d->''') == """'d"
        <=abc=>
        "d'"""
        assert tokenizer.stringify('''<-d<=<<=
        abc=>>=>d->''') == """'d"<=
        abc=>"d'"""
        assert tokenizer.stringify('''<-d<=<<=abc
        =>>=>d->''') == """'d"<=abc
        =>"d'"""
        assert tokenizer.stringify('''<=a<=b<=c=>b
        <=c<=d=>c
        =>b=>a=>''') == '''"a"b"c"b
        "c"d"c
        "b"a"'''
        assert tokenizer.stringify('''<=a<=b
<=c=>b<=c<=d=>c=>b=>a=>''') == '''"a"b
"c"b"c"d"c"b"a"'''
        assert tokenizer.stringify('''<=a<=b<=c=>b<=c<=d=>c=>b=>a
=>''') == '''"a"b"c"b"c"d"c"b"a
"'''
        assert tokenizer.stringify('''<=a<=b<=c=>b<=c<=<<=
        d=>>=>c=>b=>a=>''') == '''"a"b"c"b"c"<=
        d=>"c"b"a"'''
        assert tokenizer.stringify('''<=a<=b<=c=>b<=c<=<<=d
        =>>=>c=>b=>a=>''') == '''"a"b"c"b"c"<=d
        =>"c"b"a"'''
        assert tokenizer.stringify('''<=a<=b<=c=>b<=c<=<<=d
        =>>=>c=>b=>a=>''') == '''"a"b"c"b"c"<=d
        =>"c"b"a"'''
        assert tokenizer.stringify('''<=a
<=b<=c=>b<=c<=
        <<=d=>>
        =>c=>b=>
a=>''') == '''"a
"b"c"b"c"
        <=d=>
        "c"b"
a"'''
        assert tokenizer.stringify('''<= 
        <=b=> =>''') == '''" 
        "b" "'''
        assert tokenizer.stringify('''<= 
<=b=> =>''') == '''" 
"b" "'''
        assert tokenizer.stringify('''<= <=b=>
         =>''') == '''" "b"
         "'''
        assert tokenizer.stringify('''<= <=b=>
 =>''') == '''" "b"
 "'''
        assert tokenizer.stringify('''<=<-
        =>=>''') == '''"<-
        "=>'''
        assert tokenizer.stringify('''<=<-
=>=>''') == '''"<-
"=>'''
        assert tokenizer.stringify('''<=
        <<-=>->''') == '''"
        <-"->'''
        assert tokenizer.stringify('''<=
<<-=>->''') == '''"
<-"->'''
        assert tokenizer.stringify('''<=<-=>
        ->''') == '''"<-"
        ->'''
        assert tokenizer.stringify('''<=<-=>
->''') == '''"<-"
->'''
        assert tokenizer.stringify('''<-

<=<-=>
->''') == """'

"<-"
'"""
        assert tokenizer.stringify('''<-<=<-=>

->''') == """'"<-"

'"""
        assert tokenizer.stringify('''<-<=<-=>->
        ->''') == """'"<-"'
        ->"""
        assert tokenizer.stringify('''<-
        <=<-=>
        ->->''') == """'
        "<-"
        '->"""
        assert tokenizer.stringify('''<-<-
        =>->''') == """<-'
        =>'"""
        assert tokenizer.stringify('''<-
        <-=>
        ->''') == """<-
        '=>
        '"""
        assert tokenizer.stringify('''<=
        <-->''') == """<=
        ''"""
        assert tokenizer.stringify('''<=<-
->''') == """<='
'"""
        assert tokenizer.stringify('''<-a
        bc->''') == """'a
        bc'"""
        assert tokenizer.stringify('''<-
abc
->''') == """'
abc
'"""
        assert tokenizer.stringify('''
<+a+>''') == '''
b'''
        assert tokenizer.stringify('''<+
        a+>''') == '''<+
        a+>'''
        assert tokenizer.stringify('''
<+q+>''') == '''
true'''
        assert tokenizer.stringify('''<+q
        +>''') == '''<+q
        +>'''
        assert tokenizer.stringify('''<+r+>
''') == '''false
'''
        assert tokenizer.stringify('''<+
r
+>''') == '''<+
r
+>'''
        assert tokenizer.stringify('''
        <+s+>''') == '''
        null'''
        assert tokenizer.stringify('''<+
        s
        +>''') == '''<+
        s
        +>'''
        assert tokenizer.stringify('''<+t+>
        ''') == '''
        '''
        assert tokenizer.stringify('''<+
t+>''') == '''<+
t+>'''
        assert tokenizer.stringify('''
        <+t+>
        ''') == '''
        
        '''
        assert tokenizer.stringify('''
<+t+>
        ''') == '''

        '''
        assert tokenizer.stringify('''
        <+u+>''') == '''
        true'''
        assert tokenizer.stringify('''<+u
        +>''') == '''<+u
        +>'''
        assert tokenizer.stringify('''
        <+v+>''') == '''
        false'''
        assert tokenizer.stringify('''<+v
        +>''') == '''<+v
        +>'''
        assert tokenizer.stringify('''<+w
        +>''') == '''<+w
        +>'''
        assert tokenizer.stringify('''
        <+w+>''') == '''
        null'''
        assert tokenizer.stringify('''<-
        <+a+>->''') == """'
        b'"""
        assert tokenizer.stringify('''<-<+
        a+>->''') == """'<+
        a+>'"""
        assert tokenizer.stringify('''<=<+a+>
=>''') == '''"b
"'''
        assert tokenizer.stringify('''<=<+
        a
        +>=>''') == '''"<+
        a
        +>"'''
        assert tokenizer.stringify('''<-
        <=<+a+>=>
        ->''') == """'
        "b"
        '"""
        assert tokenizer.stringify('''<-<=<+
        a+>=>->''') == """'"<+
        a+>"'"""
        assert tokenizer.stringify('''
        <+b+>''') == '''
        <+b+>'''
        assert tokenizer.stringify('''<+
        b+>''') == '''<+
        b+>'''
        assert tokenizer.stringify('''<-<+b+>
->''') == """'<+b+>
'"""
        assert tokenizer.stringify('''<-<+
b
+>->''') == """'<+
b
+>'"""
        assert tokenizer.stringify('''
        <+c+>''') == '''
        <==>'''
        assert tokenizer.stringify('''<+c
        +>''') == '''<+c
        +>'''
        assert tokenizer.stringify('''<=<+c+>
        =>''') == '''"<==>
        "'''
        assert tokenizer.stringify('''<=<+c
        +>=>''') == '''"<+c
        +>"'''
        assert tokenizer.stringify('''<+m+>
        ''') == '''n<==>o
        '''
        assert tokenizer.stringify('''<+
m
+>''') == '''<+
m
+>'''
        assert tokenizer.stringify('''<=<+d+>=>
''') == '''"ef"
'''
        assert tokenizer.stringify('''<=<+d
+>=>''') == '''"<+d
+>"'''
        assert tokenizer.stringify("""''
        <+g'h'+>""") == """''
        i"""
        assert tokenizer.stringify("""''<+g'
h'+>""") == """''<+g'
h'+>"""
        assert tokenizer.stringify('''
<+g<-h->+>
''') == '''
i
'''
        assert tokenizer.stringify('''<+g<-h->
        +>''') == '''<+g'h'
        +>'''
        assert tokenizer.stringify('''<+j+>
''') == '''k+>>l
'''
        assert tokenizer.stringify('''<+
j
+>''') == '''<+
j
+>'''
        assert tokenizer.stringify('''<=<+j+>
=>''') == '''"k+>>l
"'''
        assert tokenizer.stringify('''
<=<+
        j+>=>
''') == '''
"<+
        j+>"
'''
        assert tokenizer.stringify('''<-
*->''') == """'
*'"""
        assert tokenizer.stringify('''
<-*->
''') == """
'*'
"""
        assert tokenizer.stringify('''
<= * =>''') == '''
" * "'''
        assert tokenizer.stringify('''<= 
*
 =>''') == '''" 
*
 "'''
        assert tokenizer.stringify('''<= <-*
        -> =>''') == '''" '*
        ' "'''
        assert tokenizer.stringify('''<= 
        <-*-> =>''') == '''" 
        '*' "'''
        assert tokenizer.stringify('''<=public=>
.foo''') == '''"public"
.foo'''
        assert tokenizer.stringify('''<=public=>.
foo''') == '''"public".
foo'''
        assert tokenizer.stringify('''
        "<~3~>"''') == '''
        3'''
        assert tokenizer.stringify('''"<~
        3~>"''') == '''
        3'''
        assert tokenizer.stringify('''"<
        ~3~>"''') == '''"<
        ~3~>"'''
        assert tokenizer.stringify('''"
        <<~3~>>"''') == '''"
        <~3~>"'''
        assert tokenizer.stringify('''"<
        <~3~>>"''') == '''"<
        <~3~>"'''
        assert tokenizer.stringify('''"<
        <~3~>>>"''') == '''"<
        <~3~>>"'''
        assert tokenizer.stringify('''"<<<~3~>>>
        "''') == '''"<<~3~>>
        "'''
        assert tokenizer.stringify('''"<<
        <~3~>
        >>"''') == '''"<<
        <~3~>
        >>"'''
        assert tokenizer.stringify('''a
        "<<~3~>>"
        b''') == '''a
        "<~3~>"
        b'''
        assert tokenizer.stringify('''a"<
        <~3~
        >>"b''') == '''a"<
        <~3~
        >>"b'''
        assert tokenizer.stringify('''<~3~>
        ''') == '''<~3~>
        '''
        assert tokenizer.stringify('''<~3
        ~>''') == '''<~3
        ~>'''
        assert tokenizer.stringify("""
        '<~true~>'""") == """
        true"""
        assert tokenizer.stringify("""'
        <~true~>'""") == """'
        <~true~>'"""
        assert tokenizer.stringify("""
        '<~<+q+>~>'""") == """
        true"""
        assert tokenizer.stringify("""'<~<
        +q+>~>'""") == """<
        +q+>"""
        assert tokenizer.stringify('''<-0
<=1
"<~3~>"2=>
4->''') == """'0
"1
32"
4'"""
        assert tokenizer.stringify('''<-0<=1"<~3
~>"2=>4->''') == """'0"13
2"4'"""
        assert tokenizer.stringify('''<-0<=1"<~3
~
>"2=>4->''') == """'0"1"<~3
~
>"2"4'"""
        assert tokenizer.stringify('''{
  "a": 1,
  "b": "<+p+>",
  "c": 3
}''') == '''{
  "a": 1,
  "b": "2",
  "c": 3
}'''
        assert tokenizer.stringify('''{
  "a": 1,
  "b": "<~<+p+>~>",
  "c": 3
}''') == '''{
  "a": 1,
  "b": 2,
  "c": 3
}'''
        assert tokenizer.stringify('''<+x+>''') == '''first line
second line
        third line with indent
    
    last line'''
        tokenizer = Tokenizer()
        assert tokenizer.stringify('''
<+a+>''') == '''
<+a+>'''
        assert tokenizer.stringify('''<+a+>
        ''') == '''<+a+>
        '''
        assert tokenizer.stringify('''
        <=<+a+>=>''') == '''
        "<+a+>"'''
        assert tokenizer.stringify('''<=<+
        a
        +>=>''') == '''"<+
        a
        +>"'''
        assert tokenizer.stringify('''<=
<+a+>
=>''') == '''"
<+a+>
"'''

    def test_stringify_template(self):
        bindings = {'precreate': 'true',
                    'id_type': 'integer',
                    'name_type': 'unicode'}
        constants = {'purge': False, 'culture': 'ru-RU'}
        t = {
          "name": "Template",
          "version": "1.0",
          "culture": "<+culture+>",
          "entities": [
            {
              "$type": "LocalEntity",
              "name": "test_template",
              "annotations": [
                {
                  "name": "precreateEntity",
                  "value": "<~{{ precreate }}~>"
                },
                {
                  "name": "purgeExistingData",
                  "value": "<~<+purge+>~>"
                }
              ],
              "attributes": [
                {
                  "name": "id",
                  "dataType": "{{ id_type }}"
                },
                {
                  "name": "name",
                  "dataType": "{{ name_type }}"
                }
              ]
            }
          ]
        }
        query = Query(raw=t, bindings=bindings)
        tokenizer = Tokenizer(constants=constants)
        bound = query.bound
        print(1048, bound)
        dumped = json.dumps(bound)
        print(1050, dumped)
        stringified = tokenizer.stringify(dumped)
        print(1052, stringified)
        assert json.loads(stringified) == {
          "name": "Template",
          "version": "1.0",
          "culture": "ru-RU",
          "entities": [
            {
              "$type": "LocalEntity",
              "name": "test_template",
              "annotations": [
                {
                  "name": "precreateEntity",
                  "value": True
                },
                {
                  "name": "purgeExistingData",
                  "value": False
                }
              ],
              "attributes": [
                {
                  "name": "id",
                  "dataType": "integer"
                },
                {
                  "name": "name",
                  "dataType": "unicode"
                }
              ]
            }
          ]
        }
